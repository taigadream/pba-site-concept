module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Концепт сайта БА',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'БизнесАвтоматика: Мы делаем крутые штуки' }
    ],
    titleTemplate: '%s | Бизнес Автоматика',
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  generate: {
    routes (callback) {
      const data = require('~/static/data.json')
      const posts = data.posts
      let routes = posts.map(post => `/blog/${post.id}`)
      callback(null, routes)
    }
  },
  /*
  ** Customize the progress-bar color
  */
  // loading: { color: '#007aff' },
  loading: false,
  // loading: '~components/Loading.vue',
  /*
  ** Build configuration
  */
  router: {},
  css: [
    '~assets/fonts/gilroy/gilroy.css',
    '~assets/styles/normalize.css'
  ],
  build: {
    loaders: [
      {
        test: /\.(png|jpe?g|gif|svg)$/,
        loader: 'url-loader',
        query: {
          limit: 3000, // 3 KO
          name: 'img/[name].[hash:7].[ext]'
        }
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader',
        query: {
          limit: 3000, // 3 KO
          name: 'fonts/[name].[hash:7].[ext]'
        }
      }
    ],
    vendor: ['vue-awesome-swiper', 'vee-validate'],
    /*
    ** Run ESLINT on save
    */
    extend (config, ctx) {
      if (ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  plugins: [
    '~/plugins/nuxt-swiper-plugin.js',
    '~/plugins/vee-validate.js'
  ]
}
