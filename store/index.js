import Vuex from 'vuex'
import axios from 'axios'
import values from 'lodash/values'

const store = () => {
  return new Vuex.Store({
    state: {
      'office_phone_number': '+74952212965',
      'navigation_items': [
        { link: '/', name: 'О нас', icon: 'face' },
        { link: 'portfolio', name: 'Работы', icon: 'work' },
        { link: 'dev-gis', name: 'ГИС', icon: 'location-on' },
        { link: 'blog', name: 'Блог', icon: 'format-align-left' },
        { link: 'contacts', name: 'Контакты', icon: 'contacts' },
        { link: 'sandbox', name: 'Test', icon: 'format-color' }
      ],
      'feedback': []
    },
    mutations: {
      setPosts (state, posts) { state.posts = posts },
      setPortfolio (state, portfolioItems) { state.portfolioItems = portfolioItems },
      setOnboard (state, onboardingSlides) { state.onboardingSlides = onboardingSlides },
      addFeedback (state, feedback) { state.feedback.push(feedback) }
    },
    actions: {
      async nuxtServerInit ({commit}) {
        let {data} = await axios.get('http://localhost:3000/data.json')
        commit('setPosts', values(data.posts))
        commit('setPortfolio', values(data.portfolioItems))
        commit('setOnboard', values(data.onboardingSlides))
      }
    },
    getters: {
      findPost: (state, getters) => (id) => {
        return state.posts.find(post => post.id === parseInt(id))
      }
    }
  })
}

export default store
